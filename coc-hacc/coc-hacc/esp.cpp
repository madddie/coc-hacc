﻿#include "inc.h"

void ESP::draw_actor_skeleton(Actor* actor, const XMVECTORF32& color)
{
    auto draw_bone = [&](const std::string& b2, const std::string& b)
    {
        vec3_t bone = actor->get_script_object()->get_bone_pos(b);
        vec3_t bone2 = actor->get_script_object()->get_bone_pos(b2);
        vec2_t scr{}, scr2{};

        if (g_ctx.world_to_screen(bone, scr) && g_ctx.world_to_screen(bone2, scr2))
        {
            g_render.line(scr.x, scr.y, scr2.x, scr2.y, color);
        }
    };

    // central skeleton
    draw_bone(BoneNames::head, BoneNames::neck);
    draw_bone(BoneNames::neck, BoneNames::spine);
    draw_bone(BoneNames::spine, BoneNames::pelvis);

    // connect neck to arms
    draw_bone(BoneNames::neck, BoneNames::LEFT::upperarm);
    draw_bone(BoneNames::neck, BoneNames::RIGHT::upperarm);

    // left arm
    draw_bone(BoneNames::LEFT::upperarm, BoneNames::LEFT::forearm);
    draw_bone(BoneNames::LEFT::forearm, BoneNames::LEFT::hand);
    draw_bone(BoneNames::LEFT::hand, BoneNames::LEFT::finger);

    // right arm
    draw_bone(BoneNames::RIGHT::upperarm, BoneNames::RIGHT::forearm);
    draw_bone(BoneNames::RIGHT::forearm, BoneNames::RIGHT::hand);
    draw_bone(BoneNames::RIGHT::hand, BoneNames::RIGHT::finger);

    // connect pelvis to legs
    draw_bone(BoneNames::pelvis, BoneNames::LEFT::thigh);
    draw_bone(BoneNames::pelvis, BoneNames::RIGHT::thigh);

    // left leg
    draw_bone(BoneNames::LEFT::thigh, BoneNames::LEFT::calf);
    draw_bone(BoneNames::LEFT::calf, BoneNames::LEFT::foot);
    draw_bone(BoneNames::LEFT::foot, BoneNames::LEFT::toe);

    // right leg
    draw_bone(BoneNames::RIGHT::thigh, BoneNames::RIGHT::calf);
    draw_bone(BoneNames::RIGHT::calf, BoneNames::RIGHT::foot);
    draw_bone(BoneNames::RIGHT::foot, BoneNames::RIGHT::toe);
}

void ESP::draw_actor(Actor* actor)
{
    static auto emoji_font = g_dwrite.get_font("Tahoma", 16);
    static auto font = g_dwrite.get_font("Operator Mono", 9);
    static auto hpfont = g_dwrite.get_font("Small Fonts", 8);

    constexpr XMVECTORF32 col_enemy = {.9f, .3f, .3f, .8f};
    constexpr XMVECTORF32 col_neutral = {.9f, .9f, .3f, .8f};
    constexpr XMVECTORF32 col_friend = {.3f, .3f, .9f, .8f};
    constexpr XMVECTORF32 col_unk = {.3f, .3f, .3f, .3f};

    XMVECTORF32 col = col_unk;

    std::wstring indicator = L"⚪";

    if (!_safe_ptr(actor))
        return;

    if (!_safe_ptr(actor->get_skeleton()))
        return;

    if (!actor->is_alive())
    {
        vec2_t scr;

        if (g_ctx.world_to_screen(actor->get_position(), scr))
        {
            g_dwrite.text(scr.x, scr.y, align_t::CENTER, {.8f, .8f, .8f, .8f}, emoji_font, L"☠");
        }
        return;   
    }

    esp_box_t box;
    if (get_2d_bbox(actor, box))
    {
        switch (g_ctx.get_relation(actor->get_inventory_owner(), g_ctx.get_local_actor()->get_inventory_owner()))
        {
        case ERelationType::eRelationTypeEnemy:
            col = col_enemy;
            indicator = L"🔻";
            break;
        case ERelationType::eRelationTypeFriend:
            col = col_friend;
            indicator = L"💙";
            break;
        case ERelationType::eRelationTypeNeutral:
            col = col_neutral;
            indicator = L"⚪";
            break;
        }

        //skeltal (dont waste time drawing this on small boxes
        if(box.h > 20)
            draw_actor_skeleton(actor, {.6f, .6f, .6f, .6f});

        g_render.outlined_box(box.x, box.y, box.w, box.h, col);

        float hp = actor->get_health(), max_hp = actor->get_max_health();
        XMVECTORF32 bar_color
        {
            math::min<float>((2.f * (max_hp - hp)) / max_hp, 1.f),
            math::min<float>((2.f * hp) / max_hp, 1.f),
            0.f,
            .8f
        };

        {
            float x = box.x, y = box.y + 1.f, h = box.h - 2.f;
            auto size = std::round(h / max_hp * hp);

            g_render.quad(x - 6, y - 3, 4, h + 5, {.05f, .05f, .05f, .8f});
            g_render.quad(x - 5, y + (h - size) - 2, 2, size + 3, bar_color);

            g_dwrite.text(box.x + (box.w * 0.5f), box.y - 36, align_t::CENTER, {.8f, .8f, .8f, .8f}, emoji_font, indicator);
            g_dwrite.text(box.x + (box.w * 0.5f), box.y - 14, align_t::CENTER, {.8f, .8f, .8f, .8f}, font, actor->get_name());

            //auto cock = actor->get_script_object()->get_bone_pos(BoneNames::pelvis);
            //vec2_t cock_scr{};
            //          
            //if (g_ctx.world_to_screen(cock, cock_scr))
            //{
            //    g_dwrite.text(cock_scr.x, cock_scr.y, align_t::CENTER, Colors::White, emoji_font, L"🍆");
            //}
            std::wstringstream bla;
            bla << std::hex << actor;

            g_dwrite.text(box.x + (box.w * 0.5f), box.y + box.h + 6, align_t::CENTER, {.8f, .8f, .8f, .8f}, font, actor->get_script_object()->get_faction());
            g_dwrite.text(box.x + (box.w * 0.5f), box.y + box.h + 20, align_t::CENTER, {.8f, .8f, .8f, .8f}, font, actor->get_class_id_to_name());
            g_dwrite.text(box.x + (box.w * 0.5f), box.y + box.h + 34, align_t::CENTER, {.8f, .8f, .8f, .8f}, font, bla);

            if (hp != max_hp)
            {
                std::stringstream fuckyou;
                fuckyou << std::dec << int(hp * 100.f);

                g_dwrite.text(x - 5, y + (h - size) - 7, align_t::CENTER, {.8f, .8f, .8f, .8f}, font, fuckyou.str());
            }
        }

    }
}

void ESP::reset()
{
    m_actors.clear();
}

void ESP::think()
{
    static auto font = g_dwrite.get_font(STR("Operator Mono"), 12);

    if (!_safe_ptr(g_global_vars.m_level))
        return;

    if (!_safe_ptr(*g_global_vars.m_level))
        return;

    auto cur_ent = g_ctx.get_level()->get_cur_ent();
    if (!_safe_ptr(cur_ent))
        return;

    auto memory = cur_ent->get_memory();
    if (!_safe_ptr(memory))
        return;

    // godm0de
    cur_ent->get_script_object()->set_health(1.f);

    // lol test
    // it worked
    if (GetAsyncKeyState(VK_F6) & 1)
    {
        auto io = cur_ent->get_inventory_owner();
        if (_safe_ptr(io))
        {
            io->set_money(10000000);
        }
    }

    // speedhacc
    static auto default_coef = cur_ent->get_run_coef();

    if (GetAsyncKeyState('C'))
        cur_ent->set_run_coef(default_coef * 3.f);
    else
        cur_ent->set_run_coef(default_coef);

    // remove old invalid entities
    purge_entities();

    // sort the ents
    sort_entities();

    // remove duplicates
    std::sort(m_actors.begin(), m_actors.end());
    m_actors.erase(std::unique(m_actors.begin(), m_actors.end()), m_actors.end());

    for (auto ent : m_actors)
    {
        draw_actor(ent);
    }

    // draw debug stats
    std::wstringstream ents;
    ents << L"⚙ ent_count: " << m_actors.size();

    // draw em
    g_dwrite.text(g_render.get_width() - 10, 100, align_t::RIGHT, Colors::FloralWhite, font, ents);

    // clear off the vectors of ents
    //reset();
}

void ESP::purge_entities()
{
    std::remove_if(m_actors.begin(), m_actors.end(), [](Actor* a)
    {
        if (!_safe_ptr(a))
            return true;

        if (!a->get_condition())
            return true;

        if (!a->get_script_object())
            return true;

        //if (!a->is_alive())
        //    return true;

        if (!_safe_ptr(a->get_skeleton()))
            return true;
        
        if (a->get_classname().length() >= 128)
            return true;

        return false;
    });
}

void ESP::sort_entities()
{
    auto memory = g_ctx.get_level()->get_cur_ent()->get_memory();

    for (int i = 1; i < 128; i++)
    {
        auto actor = memory->get_actor(i);
        if (!_safe_ptr(actor))
            return;

        if(!actor->get_script_object())
            continue;

        //if (!actor->is_alive())
        //    continue;

        m_actors.push_back(actor);
    }
}

bool ESP::get_2d_bbox(Actor *actor, esp_box_t & xywh)
{
    vec3_t                  origin, mins, maxs, hitbox_mins, hitbox_maxs;
    std::array< vec3_t, 8 > bounds;
    vec2_t                  screen;
    viewmatrix_t            matrix{};

    // grab origin and bounds.
    origin = actor->get_position();

    // get collision bounds.
    mins = actor->get_skeleton()->get_bbox().get_mins();
    maxs = actor->get_skeleton()->get_bbox().get_maxs();

    // get bounds for this object.
    bounds[0] = {mins.x, mins.y, mins.z};
    bounds[1] = {mins.x, mins.y, maxs.z};
    bounds[2] = {mins.x, maxs.y, mins.z};
    bounds[3] = {mins.x, maxs.y, maxs.z};
    bounds[4] = {maxs.x, mins.y, mins.z};
    bounds[5] = {maxs.x, mins.y, maxs.z};
    bounds[6] = {maxs.x, maxs.y, mins.z};
    bounds[7] = {maxs.x, maxs.y, maxs.z};

    // screen width / height.
    xywh.x = g_render.get_width() * 2;
    xywh.y = g_render.get_height() * 2;

    // move these off-screen.
    xywh.w = std::numeric_limits< int >::min();
    xywh.h = std::numeric_limits< int >::min();

    //
    matrix = actor->get_xform();

    // iterate bounds.
    for (auto &i : bounds)
    {
        // rotate by angles
        matrix.vec_transform(i, i);

        // convert bounds to screen coordinates.
        if (!g_ctx.world_to_screen(i, screen))
            return false;

        // compute 2d bounding box around player bounds.
        xywh.x = math::min< int >(xywh.x, (int) screen.x);
        xywh.y = math::min< int >(xywh.y, (int) screen.y);
        xywh.w = math::max< int >(xywh.w, (int) screen.x);
        xywh.h = math::max< int >(xywh.h, (int) screen.y);
    }

    xywh.w -= xywh.x;
    xywh.h -= xywh.y;

    return true;
}

bool ESP::is_actor_dormant(Actor * actor)
{
    auto memory = g_ctx.get_level()->get_cur_ent()->get_memory();

    for (int i = 1; i < 64; i++)
    {
        auto a = memory->get_actor(i);
        if (!_safe_ptr(a))
            continue;

        if (a == actor)
            return false;
    }

    return true;
}
