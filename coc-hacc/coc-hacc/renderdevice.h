#pragma once

class RenderDevice
{
private:
public:
    char pad_0000[4]; //0x0000
    uint32_t m_width; //0x0004
    uint32_t m_height; //0x0008
    char pad_000C[4]; //0x000C
    int32_t m_ready; //0x0010
    int32_t m_active; //0x0014
    uint32_t m_framecount; //0x0018
    float m_frametime; //0x001C
    float m_total_frametime; //0x0020
    char pad_0024[4]; //0x0024
    uint32_t m_time; //0x0028
    uint32_t m_time_w_pause; //0x002C
    vec3_t m_cam_pos; //0x0030
    vec3_t m_fwd; //0x003C
    vec3_t m_up; //0x0048
    vec3_t m_right; //0x0054
    viewmatrix_t m_view; //0x0060
    viewmatrix_t m_project; //0x00A0
    viewmatrix_t m_full_transform; //0x00E0
    char pad_0120[324]; //0x0120
    float m_fov; //0x0264
    float m_aspect; //0x0268
};