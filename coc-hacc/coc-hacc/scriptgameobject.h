#pragma once

class ScriptGameObject
{
protected:
private:
public:

    // walk big script init functions to get CScriptGameObject function pointers
    static Address get_script_func(const std::string& name, size_t which_ref = 0)
    {
        auto find_mov_esp_offset = [](Address op, size_t len_to_rewind = 0x40)
        {
            // todo; use crapstone to walk backwards and find 
        /*
            .text:003DDE73 C7 44 24 24 50 53 3B 00                 mov     [esp+5Ch+var_38], offset sub_3B5350  <------ script function ptr
            .text:003DDE7B 51                                      push    ecx
            .text:003DDE7C 8D 4C 24 28                             lea     ecx, [esp+60h+var_38]
            .text:003DDE80 51                                      push    ecx
            .text:003DDE81 68 80 8A 5A 00                          push    offset aIterate_invent ; "iterate_inventory"
        */

            for (auto i = op; i.as<uintptr_t>() > op.sub<uintptr_t>(len_to_rewind); --i)
            {
                auto code = i.as<uint8_t*>();

                // wow we found it
                if (
                    code[0x0] == 0xC7 &&
                    code[0x1] == 0x44 &&
                    code[0x2] == 0x24
                    )
                {
                    return i.add(0x4);
                }
            }

            // ugh why can't i just return nullptr
            return Address(nullptr);
        };

        // https://github.com/Balathruin/OXR_CoC_Demosfen/blob/c3a14e36a6fc550e4e316f4db14d95cd8b9363f5/src/xrGame/script_game_object_script2.cpp#L38
        static auto script_register_game_object1 = Pattern(HASH("xrGame.dll"), STR("83 EC 7C 53 56 57 8D 44 24 28"));
        static auto script_register_game_object1_ret = Pattern(HASH("xrGame.dll"), STR("8B C7 5F 5E 5B 83 C4 7C"));

        // https://github.com/Balathruin/OXR_CoC_Demosfen/blob/c3a14e36a6fc550e4e316f4db14d95cd8b9363f5/src/xrGame/script_game_object_script3.cpp#L59
        static auto script_register_game_object2 = Pattern(HASH("xrGame.dll"), STR("83 EC 44 53 55 56 57 8D 44 24 24"));
        static auto script_register_game_object2_ret = Pattern(HASH("xrGame.dll"), STR("5F 5E 8B C5 5D 5B 83 C4 44"));

        // search the first big funco
        auto ref1 = Stringref
        (
            script_register_game_object1,
            script_register_game_object1_ret,
            name.data(),
            which_ref
        );

        // search the second big funco
        auto ref2 = Stringref
        (
            script_register_game_object2,
            script_register_game_object2_ret,
            name.data(),
            which_ref
        );

        if (ref1)
        {
            return find_mov_esp_offset(ref1).get();
        }
        else if (ref2)
        {
            return find_mov_esp_offset(ref2).get();
        }

        return nullptr;
    }

    // CScriptGameObject::CharacterCommunity
    auto get_faction()
    {
        static auto fn = get_script_func(STR("character_community"));

        auto ret = fn.as<const char*(__thiscall*)(ScriptGameObject*)>()(this);

        return _safe_ptr(ret) ? ret : "";
    }

    auto get_ammo()
    {
        static auto fn = get_script_func(STR("ammo_get_count"));

        return fn.as<uint16_t*(__thiscall*)(ScriptGameObject*)>()(this);
    }

    auto get_ammo_max()
    {
        static auto fn = get_script_func(STR("ammo_box_size"));

        return fn.as<uint16_t*(__thiscall*)(ScriptGameObject*)>()(this);
    }

    void set_position(const vec3_t& pos)
    {
        static auto fn = get_script_func(STR("force_set_position"));

        return fn.as<void(__thiscall*)(ScriptGameObject*, vec3_t, bool)>()(this, pos, false);
    }

    void set_health(float hp)
    {
        static auto fn = get_script_func(STR("set_health_ex"));

        return fn.as<void(__thiscall*)(ScriptGameObject*, float)>()(this, hp);
    }

    bool is_bone_visible(const std::string& name)
    {
        static auto fn = get_script_func(STR("bone_visible"));

        return fn.as<bool(__thiscall*)(ScriptGameObject*)>()(this);
    }

    bool is_inside(const vec3_t& pos)
    {
        static auto fn = get_script_func(STR("inside"), 1);

        return fn.as<bool(__thiscall*)(ScriptGameObject*, const vec3_t&)>()(this, pos);
    }

    const vec3_t& get_bone_pos(const std::string& name)
    {
        static auto fn = get_script_func(STR("bone_position"));
        static vec3_t ret{};

        ret.clear();

        fn.as<void(__thiscall*)(ScriptGameObject*, vec3_t*, const char*)>()(this, &ret, name.data());

        return ret;
    }
};