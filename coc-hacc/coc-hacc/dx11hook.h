#pragma once

class DX11Hook
{
protected:

    struct dx_data_t
    {
        ID3D11Device*           m_device;
        IDXGISwapChain*         m_swap_chain;
        ID3D11DeviceContext*    m_context;
    } m_data;

public:

    std::shared_ptr<VMT> m_swapchain_vmt;

    void init();
    void hook();

    __forceinline auto get_data()
    {
        return m_data;
    }
};

extern DX11Hook g_dx11_hook;