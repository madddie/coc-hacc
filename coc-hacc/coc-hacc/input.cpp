#include "inc.h"

bool Input::init(WNDPROC new_wnd_proc)
{
    m_window = GetForegroundWindow();

    return attach(new_wnd_proc);
}

bool Input::attach(WNDPROC new_wnd_proc)
{
    m_prev_wnd_proc = (WNDPROC) SetWindowLongPtr(m_window, GWLP_WNDPROC, LONG_PTR(new_wnd_proc));

    return true;
}

void Input::detach()
{
    SetWindowLongPtr(m_window, GWLP_WNDPROC, LONG_PTR(m_prev_wnd_proc));
}

LRESULT Input::wnd_proc(uint32_t msg, WPARAM w, LPARAM l)
{
    switch (msg)
    {
    case WM_KEYDOWN:
        m_keys[w] = true;
        m_last_key = (size_t) w;
        break;
    case WM_KEYUP:
        m_keys[w] = false;
        m_last_key = (size_t) w;
        break;
    case WM_MOUSEMOVE:
        m_mouse.m_pt.x = (int) ((short) LOWORD(l));
        m_mouse.m_pt.y = (int) ((short) HIWORD(l));
        break;
    case WM_LBUTTONDOWN:
        m_mouse.m_state[LEFT_CLICKING] = true;
        break;
    case WM_LBUTTONUP:
        m_mouse.m_state[LEFT_CLICKING] = false;
        break;
    case WM_RBUTTONDOWN:
        m_mouse.m_state[RIGHT_CLICKING] = true;
        break;
    case WM_RBUTTONUP:
        m_mouse.m_state[RIGHT_CLICKING] = false;
        break;
    default:
        break;
    }

    // this should chain into input command building and swallow the input. but for now that can wait
    return true;
}
