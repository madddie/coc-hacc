#pragma once

using actors_t = std::vector<Actor*>;

struct esp_box_t
{
    int x, y, w, h;
};

class ESP
{
private:

protected:

    actors_t m_actors{};
    actors_t m_dormant{};

public:

    // clear up local variables
    void reset();

    // big thunker
    void think();

    // we need to remove deallocated entities
    // sanity check the shit out of them and 
    // remove then from the actor vector
    void purge_entities();

    // spooky skeltal
    void draw_actor_skeleton(Actor * actor, const XMVECTORF32 & color);

    // draw an actor
    // todo; add more than a fucking actor
    void draw_actor(Actor * actor);

    // categorize entities into their separate vectors 
    // todo; again, add morethan a fucking actor
    void sort_entities();

    // construct 2d bbox
    bool get_2d_bbox(Actor*, esp_box_t& xywh);

    // yea
    auto get_actors()
    {
        return m_actors;
    }

    // check if actor is no longer in our current vector
    bool is_actor_dormant(Actor* actor);

};

extern ESP g_esp;