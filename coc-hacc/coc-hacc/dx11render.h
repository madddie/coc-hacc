#pragma once

class DX11Render
{
private:

    struct d3d_data_t
    {
        uint32_t m_msaa_quality;

        ID3D11Device*           m_device;
        ID3D11DeviceContext*    m_device_context;
        ID3D11RenderTargetView* m_render_target_view;
        ID3D11Texture2D*        m_render_target_texture;
        ID3D11Buffer*           m_index_buffer;
        ID3D11Buffer*           m_vertex_buffer;
        IDXGISwapChain*         m_swap_chain;

        ID3D11BlendState*       m_alpha_on_blend_state;
        ID3D11BlendState*       m_alpha_off_blend_state;

        ID3D11RasterizerState*   m_rasterizer_state;
        ID3D11DepthStencilState* m_depth_stencil_state;

        ID3D11InputLayout*      m_input_layout;

        D3D11_BLEND_DESC        m_blend_state_desc;
        D3D11_VIEWPORT          m_viewport;

        D3D_DRIVER_TYPE         m_driver_type;

        std::unique_ptr<CommonStates>   m_common_states;
        std::unique_ptr<EffectFactory>  m_effect_factory;
        std::unique_ptr<SpriteBatch>    m_sprite_batch;
        std::unique_ptr<SpriteFont>     m_sprite_font;
        std::unique_ptr<PrimitiveBatch<VertexPositionColor>> m_prim_batch;
        std::unique_ptr<BasicEffect>    m_batch_effect;
    } m_data;


    DXTKStateSaver m_statesaver{};

    std::vector<std::function<void()>> m_callbacks;

    bool m_minimized, m_resizing, m_restore_state = false;

    float m_width, m_height;

    struct global_vars_t
    {
        double   m_frametime, m_fps, m_last;
        uint64_t m_framecount;
    } m_global_vars;

public:

    DX11Render() :
        m_data{}, m_width{}, m_height{}, m_minimized{}, m_resizing{} {}

    // create device, swapchain, blendstate
    // and various dxtk classes
    bool create();

    // handle messages
    bool wnd_proc(uint32_t msg, WPARAM w, LPARAM l);

    // need a way to reset
    void reset();

    // obvious
    void paint();

    // get task queue
    __forceinline auto& get_tasks()
    {
        return m_callbacks;
    }

    // get the statesaver
    __forceinline auto& get_statesaver()
    {
        return m_statesaver;
    }

    // add task
    __forceinline void push_task(const std::function<void()>& callback)
    {
        m_callbacks.push_back(callback);
    }

    // remove task
    __forceinline void pop_task()
    {
        m_callbacks.pop_back();
    }

    // get huge data container
    __forceinline auto& get_data()
    {
        return m_data;
    }

    __forceinline auto& get_global_vars()
    {
        return m_global_vars;
    }

    __forceinline auto* get_arial()
    {
        return m_data.m_sprite_font.get();
    }

    __forceinline auto get_width()
    {
        return m_width;
    }
    __forceinline auto get_height()
    {
        return m_height;
    }

    void begin();
    void end();


    // debug drawing functions

    // text
    void text(int x, int y, float scale, SpriteFont* font, const XMVECTORF32& color, const std::string& str);
    void text(int x, int y, float scale, SpriteFont* font, const XMVECTORF32& color, const std::stringstream& str);

    // quad
    void quad(int x, int y, int w, int h, const XMVECTORF32& color);

    // box
    void box(int x, int y, int w, int h, const XMVECTORF32& color);
    void outlined_box(int x, int y, int w, int h, const XMVECTORF32& color);
    
    // 2d line
    void line(int x, int y, int x2, int y2, const XMVECTORF32& color);

};

extern DX11Render g_render;