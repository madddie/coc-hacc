#include "inc.h"

Console g_con{"[cocc]"};

global_vars_t g_global_vars{};

Context  g_ctx{};
DX11Hook g_dx11_hook{};
DX11Render g_render{};

DirectWrite g_dwrite{};

Input g_input{};

DrawUtilities* g_draw_util{};

ESP g_esp{};

static long __stdcall window_proc(HWND h, uint32_t msg, WPARAM w, LPARAM l)
{
    switch (msg)
    {
    case WM_SIZE:
    case WM_ENTERSIZEMOVE:
    case WM_EXITSIZEMOVE:
        return g_render.wnd_proc(msg, w, l);
    case WM_MOUSEMOVE:
    case WM_KEYDOWN:
    case WM_KEYUP:
    case WM_RBUTTONDOWN:
    case WM_LBUTTONDOWN:
    case WM_LBUTTONUP:
    case WM_RBUTTONUP:
        return g_input.wnd_proc(msg, w, l);

    case WM_DESTROY:
        PostQuitMessage(EXIT_SUCCESS);
        return EXIT_SUCCESS;
    }

    return DefWindowProc(h, msg, w, l);
}

static ulong_t __stdcall init(void* hinst)
{
    g_con.print("[#] initializing\n");

    // setup basic globals and shit
    g_ctx.init();
    g_con.print("[+] context::initialized\n");

    // init input
    g_input.init(window_proc);
    g_con.print("[+] input::attached\n");

    while (true)
    {
        if (GetAsyncKeyState(VK_F12) & 1)
        {
            g_con.print("\n\n[+] reinit ...\n");
        }
    }

    return 1;
}

int __stdcall DllMain(HINSTANCE hinst, int reason, void* reserved)
{
    if (reason == DLL_PROCESS_ATTACH)
    {
        auto handle = CreateThread(0, 0, init, hinst, 0, 0);

        // ?
        //WaitForSingleObject(handle, INFINITE);

        CloseHandle(handle);

        return 1;
    }
    
    return 0;
}