#pragma once

using color = uint32_t;

namespace colors
{
    constexpr color make(uint8_t r = 0, uint8_t g = 0, uint8_t b = 0, uint8_t a = 0)
    {
        return ((color) ((((a) & 0xff) << 24) | (((r) & 0xff) << 16) | (((g) & 0xff) << 8) | ((b) & 0xff)));
    }

    constexpr auto red = make(255, 0, 0, 255);
    constexpr auto green = make(0, 255, 0, 255);
    constexpr auto blue = make(0, 0, 255, 255);
};
