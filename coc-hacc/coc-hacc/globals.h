#pragma once

struct global_vars_t
{
    Actor** m_actor;
    GameLevel** m_level;
    RenderDevice** m_render_device;

    global_vars_t() :
        m_actor{nullptr},
        m_level{nullptr},
        m_render_device{nullptr} 
    {}
};

extern global_vars_t g_global_vars;

