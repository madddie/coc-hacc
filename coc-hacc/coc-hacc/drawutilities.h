#pragma once

class DrawUtilities
{
private:

protected:

public:

    enum INDICES
    {
        DRAWLINE = 15,
    };

    void draw_line(float x1, float y1, float x2, float y2, color clr);

};

extern DrawUtilities* g_draw_util;