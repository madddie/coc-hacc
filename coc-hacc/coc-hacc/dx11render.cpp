#include "inc.h"

bool DX11Render::create()
{
    m_data.m_common_states = std::make_unique<CommonStates>(m_data.m_device);
    m_data.m_sprite_batch = std::make_unique<SpriteBatch>(m_data.m_device_context);
    m_data.m_effect_factory = std::make_unique<EffectFactory>(m_data.m_device);
    m_data.m_prim_batch = std::make_unique<PrimitiveBatch<VertexPositionColor>>(m_data.m_device_context);

    m_data.m_batch_effect = std::make_unique<BasicEffect>(m_data.m_device);
    m_data.m_batch_effect->SetVertexColorEnabled(true);

    const void* shader_code;
    size_t code_len;

    m_data.m_batch_effect->GetVertexShaderBytecode(&shader_code, &code_len);

    if (S_OK != m_data.m_device->CreateInputLayout(VertexPositionColor::InputElements, VertexPositionColor::InputElementCount, shader_code, code_len, &m_data.m_input_layout))
        return false;

    reset();

    return true;
}

bool DX11Render::wnd_proc(uint32_t msg, WPARAM w, LPARAM l)
{
    switch (msg)
    {
    case WM_SIZE:
        if (m_data.m_device)
        {
            m_width = (int) LOWORD(l);
            m_height = (int) HIWORD(l);

            if (w == SIZE_MINIMIZED)
                m_minimized = true;
            else if (w == SIZE_MAXIMIZED)
            {
                m_minimized = false;
                reset();
            }
            else if (w == SIZE_RESTORED)
            {
                if (m_minimized)
                {
                    m_minimized = false;
                    reset();
                }
                else if (m_resizing)
                {

                }
                else
                {
                    reset();
                }
            }
        }
        return 0;

    case WM_ENTERSIZEMOVE:
        m_resizing = true;
        return 0;

    case WM_EXITSIZEMOVE:
        m_resizing = false;
        reset();
        return 0;

    }

    return 0;
}

// credits
// dex
void DX11Render::reset()
{
    static float					lastW{}, lastH{};
    D3D11_VIEWPORT&					viewport = m_data.m_viewport;
    
    if (m_width != lastW || m_height != lastH) 
    {
        g_con.print("[=] resize -> %f, %f\n", m_width, m_height);
    
        //g_render.get_data().m_swap_chain->ResizeBuffers(0, m_width, m_width, DXGI_FORMAT_B8G8R8A8_UNORM_SRGB, 0);

        // set our view projection.
        m_data.m_batch_effect->SetProjection(XMMatrixOrthographicOffCenterRH(0.f, m_width, m_height, 0.f, 0.f, 1.f));
        m_data.m_batch_effect->SetWorld(XMMatrixIdentity());
        m_data.m_batch_effect->SetView(XMMatrixIdentity());
    
        // setup viewport and set.
        viewport.TopLeftX = 0.f;
        viewport.TopLeftY = 0.f;
        viewport.Width = m_width;
        viewport.Height = m_height;
        viewport.MinDepth = 0.f;
        viewport.MaxDepth = 1.f;
        m_data.m_device_context->RSSetViewports(1, &viewport);
    }
    
    lastW = m_width;
    lastH = m_height;
}

void DX11Render::paint()
{
  
}

void DX11Render::text(int x, int y, float scale, SpriteFont* font, const XMVECTORF32& color, const std::stringstream& str)
{
    text(x, y, scale, font, color, str.str());
}

void DX11Render::begin()
{
    DXGI_SWAP_CHAIN_DESC	desc{};
    WINDOWINFO				info{};
    D3D11_VIEWPORT          viewport{};

    // save state
    m_restore_state = false;
    m_statesaver.saveCurrentState(m_data.m_device_context);
    m_restore_state = true;

    // get swapchain desc
    m_data.m_swap_chain->GetDesc(&desc);

    // get the windows info
    GetWindowInfo(desc.OutputWindow, &info);

    // save w/h
    m_width = (float) (info.rcClient.right - info.rcClient.left);
    m_height = (float) (info.rcClient.bottom - info.rcClient.top);

    // handle resize
    reset();

    // set viewport
    viewport.TopLeftX = 0.f;
    viewport.TopLeftY = 0.f;
    viewport.Width = m_width;
    viewport.Height = m_height;
    viewport.MinDepth = 0.f;
    viewport.MaxDepth = 1.f;
    m_data.m_device_context->RSSetViewports(1, &viewport);

    // 
    m_data.m_batch_effect->Apply(m_data.m_device_context);
    m_data.m_device_context->IASetInputLayout(m_data.m_input_layout);    

    m_data.m_sprite_batch->Begin();
}

void DX11Render::end()
{
    m_data.m_sprite_batch->End();

    if (m_restore_state)
        m_statesaver.restoreSavedState();
}

void DX11Render::text(int x, int y, float scale, SpriteFont* font, const XMVECTORF32& color, const std::string& str)
{
    XMFLOAT2 pos{(float) x, (float) y};

    font->DrawString(m_data.m_sprite_batch.get(), util::multibyte_to_wide(str).c_str(), pos, color, 0.f, {0.f, 0.f}, scale);
}

void DX11Render::quad(int x, int y, int w, int h, const XMVECTORF32& color)
{
    float tmpx = (float) x-1;
    float tmpy = (float) y;
    float tmpw = (float) w;
    float tmph = (float) h;

    std::array<VertexPositionColor, 4> verts
    {
        {
            {{tmpx, tmpy}, color},
            {{tmpx + tmpw, tmpy}, color},
            {{tmpx + tmpw, tmpy + tmph}, color},
            {{tmpx, tmpy + tmph}, color},
        }
    };

    m_data.m_prim_batch->Begin();
    m_data.m_prim_batch->DrawQuad(verts[0], verts[1], verts[2], verts[3]);
    m_data.m_prim_batch->End();
}

void DX11Render::box(int x, int y, int w, int h, const XMVECTORF32 & color)
{
    float tmpx = (float) x-1;
    float tmpy = (float) y;
    float tmpw = (float) w;
    float tmph = (float) h;

    std::array<VertexPositionColor, 5> verts
    {
        {
            {{tmpx, tmpy}, color},
            {{tmpx + tmpw, tmpy}, color},
            {{tmpx + tmpw, tmpy + tmph}, color},
            {{tmpx, tmpy + tmph}, color},
            {{tmpx, tmpy - 1}, color}
        }
    };

    m_data.m_prim_batch->Begin();
    m_data.m_prim_batch->Draw(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP, verts.data(), 5);
    m_data.m_prim_batch->End();
}

void DX11Render::outlined_box(int x, int y, int w, int h, const XMVECTORF32 & color)
{
    static XMVECTORF32 outline = {.15f, .15f, .15, .75f};

    // outlines
    box(x - 1, y - 1, w + 2, h + 2, outline);
    box(x + 1, y + 1, w - 2, h - 2, outline);

    // inner
    box(x, y, w, h, color);
}

void DX11Render::line(int x, int y, int x2, int y2, const XMVECTORF32 & color)
{
    float tmpx = (float)x;
    float tmpy = (float)y;
    float tmpx2 = (float)x2;
    float tmpy2 = (float)y2;

    std::array<VertexPositionColor, 2> verts
    {
        {
            {{tmpx, tmpy}, color},
            {{tmpx2, tmpy2}, color},
        }
    };

    m_data.m_prim_batch->Begin();
    m_data.m_prim_batch->DrawLine(verts[0], verts[1]);
    m_data.m_prim_batch->End();
}

