#include "inc.h"

Context::Context()
{
}

Context::~Context()
{
}

void Context::init()
{
    // echo out that we r scanning
    g_con.print("[#] scanning for offsets :B\n");

    // grab da globs
    g_global_vars.m_actor         = Pattern(HASH("xrGame.dll"), STR("83 3D ? ? ? ? ? 0F 84 ? ? ? ? E8")).add(2).get<Actor**>(1);
    g_global_vars.m_level         = Pattern(HASH("xrGame.dll"), STR("A1 ? ? ? ? 8B 00 39 B0 ? ? ? ? 0F 85")).add(1).get<GameLevel**>(2);
    g_global_vars.m_render_device = Pattern(HASH("xrGame.dll"), STR("A1 ? ? ? ? FF 70 18 0F B7 C7 50 8B 45 00")).add(1).get<RenderDevice**>(1);
    
    // grab da intafacez
    g_draw_util                   = Pattern(HASH("xrEngine.exe"), STR("A1 ? ? ? ? 56 57 8B F9 8B 00")).add(1).get<DrawUtilities*>(3);

    // prunt
    g_con.print("[+] globals::m_actor 0x%X\n", g_global_vars.m_actor);
    g_con.print("[+] globals::m_level 0x%X\n", g_global_vars.m_level);
    g_con.print("[+] globals::m_render_device 0x%X\n", g_global_vars.m_render_device);
    g_con.print("[+] interfaces::g_draw_util 0x%X\n", g_draw_util);

    // hook dx11
    g_dx11_hook.init();

    g_render.push_task([ & ]()
    {
        g_esp.think();
    });

}

Actor* Context::get_local_actor()
{
    return *g_global_vars.m_actor;
}

GameLevel* Context::get_level()
{
    return *g_global_vars.m_level;
}

RenderDevice* Context::get_render_device()
{
    return *g_global_vars.m_render_device;
}

ERelationType Context::get_relation(InventoryOwner * a, InventoryOwner * b)
{
    if (!_safe_ptr(a) || !_safe_ptr(b))
        return ERelationType::eRelationTypeDummy;

    static auto call_get_relation = Pattern(HASH("xrGame.dll"), STR("E8 ? ? ? ? 8D 4C 24 24 8B F0")).rel(1);

    return call_get_relation.as<ERelationType(__stdcall*)(InventoryOwner*, InventoryOwner*)>()(a, b);
}


bool Context::is_culled(const vec3_t & p)
{
    auto device = *g_global_vars.m_render_device;
    vec3_t c;

    if (!Address::safe(device))
        return false;

    auto width = device->m_width, height = device->m_height;
    viewmatrix_t& matrix = device->m_full_transform;

    float w = p.x * matrix._14 + p.y * matrix._24 + p.z * matrix._34 + matrix._44;

    return (w < 0.f);
}

bool Context::world_to_screen(const vec3_t & p, vec2_t & screen)
{
    auto device = *g_global_vars.m_render_device;
    vec3_t c;

    if (!Address::safe(device))
        return false;

    auto width = device->m_width, height = device->m_height;
    viewmatrix_t& matrix = device->m_full_transform;

    float w = p.x * matrix._14 + p.y * matrix._24 + p.z * matrix._34 + matrix._44;
    if (w < 0.f)
        return false; // culling

    matrix.transform(c, p);

    screen.x = (1.f + c.x) / 2.f * (width);
    screen.y = (1.f + (-c.y)) / 2.f * (height);

    return true;
}
