﻿#include "inc.h"

HRESULT __stdcall present_hook(IDXGISwapChain* ecx, uint32_t sync, uint32_t flags)
{
    static std::once_flag flag;

    std::call_once(flag, [&]()
    {
        g_con.print("[=] grabbing dx data from %s\n", __FUNCTION__);

        auto& data = g_render.get_data();

        // grab the games device
        ecx->GetDevice(__uuidof(data.m_device), (void**)&data.m_device);

        // grab the games context
        data.m_device->GetImmediateContext(&data.m_device_context);

        // grab render target
        ecx->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**) &data.m_render_target_texture);
        data.m_device->CreateRenderTargetView(data.m_render_target_texture, nullptr, &data.m_render_target_view);
        data.m_render_target_texture->Release();

        // print out the stuf
        g_con.print("[+] dx11::device ... 0x%X\n", data.m_device);
        g_con.print("[+] dx11::context ... 0x%X\n", data.m_device_context);
        g_con.print("[+] dx11::swapchain ... 0x%X\n", data.m_swap_chain);

        // initialize the dxtk renderer with the games dx globals
        g_render.create();

        // init and attach directwrite
        g_dwrite.init();
    });

    g_render.get_data().m_device_context->OMSetRenderTargets(1, &g_render.get_data().m_render_target_view, nullptr);
    g_render.begin();

    // watermark//
    g_dwrite.text( g_render.get_width() - 10, 10, align_t::RIGHT, Colors::White, g_dwrite.get_font("Tahoma", 20), L"🇸.🇹.🇦.🇱.🇰.🇪.🇷. 🌸");

    // iterate and execute per-frame callbacks
    for (auto& callback : g_render.get_tasks())
    {
        callback();
    }
  
    g_render.end();
    return g_dx11_hook.m_swapchain_vmt->get_method<HRESULT(__stdcall*)(IDXGISwapChain*, int, int)>(8)(ecx, sync, flags);
}

void DX11Hook::init()
{
    D3D_FEATURE_LEVEL feature_level = D3D_FEATURE_LEVEL_11_0;
    DXGI_SWAP_CHAIN_DESC swapchain_desc{};

    swapchain_desc.BufferCount = 1;
    swapchain_desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    swapchain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapchain_desc.OutputWindow = GetForegroundWindow();
    swapchain_desc.SampleDesc.Count = 1;
    swapchain_desc.Windowed = (GetWindowLong(GetForegroundWindow(), GWL_STYLE) & WS_POPUP) != 0 ? FALSE : TRUE;
    swapchain_desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    swapchain_desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    swapchain_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

    D3D11CreateDeviceAndSwapChain(
        0,
        D3D_DRIVER_TYPE::D3D_DRIVER_TYPE_HARDWARE,
        0,
        0,
        &feature_level,
        1,
        D3D11_SDK_VERSION,
        &swapchain_desc,
        &m_data.m_swap_chain,
        &m_data.m_device,
        0,
        &m_data.m_context
    );

    return hook();
}

void DX11Hook::hook()
{
    MEMORY_BASIC_INFORMATION mbi{};
    uintptr_t* game_swapchain, *swapchain_vtable = *(uintptr_t**) m_data.m_swap_chain;
    
    // BOOYAKA BOOYAKA!!!!!!!!!!!!!!
    game_swapchain = (uintptr_t*)Pattern::find_by_data<uintptr_t*>(0x10000, 0xFFE00000, &swapchain_vtable);

    // thine hog is large
    g_con.print(game_swapchain ? "[+] found xrEngine::IDXGISwapchain 0x%X\n" : "[-] fail. couldn't brute the swapchain\n", game_swapchain);

    // hook present... DUH!!!
    m_swapchain_vmt = std::make_shared<VMT>(game_swapchain, true);
    m_swapchain_vmt->hook_method(&present_hook, 8);
    
    // release and return
    m_data.m_device->Release();
    m_data.m_context->Release();
    m_data.m_swap_chain->Release();
    
    // set the old data for the new shtuf
    g_render.get_data().m_swap_chain = (IDXGISwapChain*)game_swapchain;

    // echo we succeeded?
    g_con.print("[+] dx11::Present hooked at 0x%X\n", m_swapchain_vmt->get_method<uintptr_t>(8));

    return;
}
