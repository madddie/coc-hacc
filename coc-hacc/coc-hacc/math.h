#pragma once

// credits
// dex, nitro
namespace math 
{
    // pi constants.
    constexpr float pi = 3.14159265358979323846f;  // M_PI
    constexpr float pi_2 = pi * 2.f;                 // M_PI * 2.f

                                                     // degrees to radians.
    constexpr float deg_to_rad(float val) {
        return val * (pi / 180.f);
    }

    // radians to degrees.
    constexpr float rad_to_deg(float val) {
        return val * (180.f / pi);
    }

    // min.
    template< typename t > __forceinline t min(float a, float b) {
        return (t) _mm_cvtss_f32(_mm_min_ss(_mm_set_ss(a), _mm_set_ss(b)));
    }

    // max.
    template< typename t > __forceinline t max(float a, float b) {
        return (t) _mm_cvtss_f32(_mm_max_ss(_mm_set_ss(a), _mm_set_ss(b)));
    }

    // clamp.
    template< typename t > __forceinline void clamp(t &val, const t &min, const t &max) {
        val = (t) _mm_cvtss_f32(_mm_min_ss(_mm_max_ss(_mm_set_ss(val), _mm_set_ss(min)), _mm_set_ss(max)));
    }

    // sqrt.
    __forceinline float sqrt(float val) {
        return _mm_cvtss_f32(_mm_sqrt_ss(_mm_set_ss(val)));
    }

    // angle mod.
    __forceinline float angle_mod(float angle) {
        return (360.f / 65536) * ((int) (angle * (65536.f / 360.0f)) & 65535);
    }

    // normalizes an angle.
    void normalize_angle(float &angle);

    __forceinline float normalized_angle(float angle) {
        normalize_angle(angle);
        return angle;
    }

    // multiply matrix
    viewmatrix_t& matrix_mul(const viewmatrix_t& A, const viewmatrix_t& B);

    // converts a vector to an angle.
    void vector_angles(const vec3_t& forward, ang_t& angles, vec3_t *up = nullptr);

    // constructs a mat3x4 from and origin and viewangles
    void matrix_from_angles(const vec3_t& origin, const ang_t& angles, matrix3x4_t& out);

    // converts an angle into it's corresponding directional vectors.
    void angle_vectors(const ang_t& angles, vec3_t* forward, vec3_t* right = nullptr, vec3_t* up = nullptr);

    // assume matrix is a rotation matrix and rotate the input vector.
    void vector_rotate(const vec3_t &in, const matrix3x4_t &matrix, vec3_t &out);

    // rotate vector by the inverse of the matrix.
    void vector_irotate(const vec3_t &in, const matrix3x4_t &matrix, vec3_t &out);

    // transform in by the matrix.
    void vector_transform(const vec3_t &in, const matrix3x4_t &matrix, vec3_t &out);

    // assuming the matrix is orthonormal, transform in by the transpose ( also the inverse in this case ) of the matrix.
    void vector_itransform(const vec3_t &in, const matrix3x4_t &matrix, vec3_t &out);

    // returns FOV.
    float get_fov(const ang_t &view_angles, const vec3_t &start, const vec3_t &end);

    // return fov
    float get_fov(const ang_t &view_angles, const ang_t &new_angles);


    // if this returns -1, it failed and we intersected nothing, otherwise it returns the face you hit.
    int intersect_ray_with_box(const vec3_t &start, const vec3_t &end_delta, const vec3_t &mins, const vec3_t &maxs, float tolerance = 0.f);

    // ...
    int intersect_ray_with_OBB(const vec3_t &start, const vec3_t &end_delta, vec3_t mins, vec3_t maxs, float radius, const matrix3x4_t &matrix);

    // ...
    // bool IntersectRayWithOBB( const vec3_t &vecRayStart, const vec3_t &vecRayDelta, const matrix3x4_t &matOBBToWorld, const vec3_t &vecOBBMins, const vec3_t &vecOBBMaxs, float flRadius );

    // ...
    bool intersect_ray_with_sphere(const vec3_t &start, const vec3_t &ray_delta, const vec3_t &sphere_center, float radius);
};