#include "inc.h"

void DrawUtilities::draw_line(float x1, float y1, float x2, float y2, color clr)
{
    using Fvector = std::array<float, 3>;
    using draw_line_t = void(__thiscall*)(DrawUtilities*, const Fvector&, const Fvector&, color);

    Fvector a{x1, y1, 0}, b{x2, y2, 0};

    return util::get_method<draw_line_t>(this, DRAWLINE)(this, a, b, clr);
}
