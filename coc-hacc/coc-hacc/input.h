#pragma once

using mouse_state_t = std::bitset<4>;
using key_state_t = std::bitset<256>;

class Input
{
private:

    enum mouse_states
    {
        LEFT_CLICKING,
        RIGHT_CLICKING,
        LEFT_CLICKED,
        RIGHT_CLICKED
    };

    struct mouse_t
    {
        POINT m_pt;
        mouse_state_t m_state;
    } m_mouse;

    key_state_t m_keys;

    size_t m_last_key;

    HWND m_window;

    WNDPROC m_prev_wnd_proc;

public:

    Input() : m_keys{}, m_mouse{}
    {

    }

    bool init(WNDPROC);
    bool attach(WNDPROC);
    void detach();

    LRESULT wnd_proc(uint32_t msg, WPARAM w, LPARAM l);

    auto get_mouse()
    {
        return m_mouse;
    }
    auto get_last_key()
    {
        return m_last_key;
    }
    bool get_key(size_t key)
    {
        return m_keys[key];
    }

};

extern Input g_input;