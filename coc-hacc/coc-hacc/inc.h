#pragma once

#include <directxmath.h>

// main overarching helper file
#include <m_maddie.h>
#include <mutex>
#include <bitset>


// ez
#define _safe_ptr(ptr) (ptr && Address::safe(ptr))

// dx11 dependencies 
#pragma comment(lib, "d3d11.lib")

// dxtk dependencies
#include "CommonStates.h"
#include "DDSTextureLoader.h"
#include "DirectXHelpers.h"
#include "Effects.h"
#include "GamePad.h"
#include "GeometricPrimitive.h"
#include "GraphicsMemory.h"
#include "Keyboard.h"
#include "Model.h"
#include "Mouse.h"
#include "PostProcess.h"
#include "PrimitiveBatch.h"
#include "ScreenGrab.h"
#include "SimpleMath.h"
#include "SpriteBatch.h"
#include "SpriteFont.h"
#include "VertexTypes.h"
#include "WICTextureLoader.h"



using namespace DirectX;

// directwrite includes
#include <D2D1.h>
#include <DWrite.h>
#pragma comment(lib, "DWrite.lib")
#pragma comment(lib, "d2d1")

// pre-declare for math.h
class viewmatrix_t;
class matrix3x4_t;
class vec3_t;
class ang_t;

// decl this
extern Console g_con;

// maths
#include "math.h"
#include "vec.h"
#include "vec2.h"
#include "ang.h"

// big
#include "input.h"

// sdk includes
#include "sdk.h"
#include "globals.h"
#include "context.h"
#include "scriptgameobject.h"

// code includes
#include "dx11statesaver.h"
#include "dx11render.h"
#include "dx11hook.h"

// directwrite
#include "directwrite.h"

// menu
#include "menu_inc.h"

// haccs
#include "esp.h"

// double tehe
#pragma warning(disable:4838)
#pragma warning(disable:4800)
#pragma warning(disable:4005)
// 
#define UI_BASE_WIDTH 1024.0f
#define UI_BASE_HEIGHT 768.0f
#define RECT_SIZE 11

// tehe!!!
#define release_com(x)\
{                     \
    if (x)            \
    {                 \
       x->Release();  \
       x = nullptr;   \
    }                 \
}   

using callback_t = std::function<void()>;
using callbacks_t = std::vector<callback_t>;