#pragma once

// credits
// dex, nitro
class vec3_t {
public:

    // data member variables
    float x, y, z;

public:
    // constructors
    __forceinline vec3_t(void) { }
    __forceinline vec3_t(float x, float y, float z) : x(x), y(y), z(z) { }

    // at-accesors
    __forceinline float& at(const size_t index) {
        return ((float*)this)[index];
    }

    __forceinline float& at(const size_t index) const {
        return ((float*)this)[index];
    }

    // index operators
    __forceinline float& operator()(const size_t index) {
        return at(index);
    }
    __forceinline const float& operator()(const size_t index) const {
        return at(index);
    }
    __forceinline float& operator[](const size_t index) {
        return at(index);
    }
    __forceinline const float& operator[](const size_t index) const {
        return at(index);
    }

    // equality operators
    __forceinline bool operator==(const vec3_t& v) const {
        return v.x == x && v.y == y && v.z == z;
    }

    __forceinline bool operator!=(const vec3_t& v) const {
        return v.x != x || v.y != y || v.z != z;
    }

    // copy assignment
    __forceinline vec3_t& operator=(const vec3_t& v) {
        x = v.x;
        y = v.y;
        z = v.z;
        return *this;
    }

    // negation-operator
    __forceinline vec3_t operator-() const {
        return vec3_t(-x, -y, -z);
    }

    // arithmetic operators
    __forceinline vec3_t operator+(const vec3_t& v) const {
        return{
            x + v.x,
            y + v.y,
            z + v.z
        };
    }
    __forceinline vec3_t operator-(const vec3_t& v) const {
        return{
            x - v.x,
            y - v.y,
            z - v.z
        };
    }
    __forceinline vec3_t operator*(const vec3_t& v) const {
        return{
            x * v.x,
            y * v.y,
            z * v.z
        };
    }
    __forceinline vec3_t operator/(const vec3_t& v) const {
        return{
            x / v.x,
            y / v.y,
            z / v.z
        };
    }

    // compound assignment operators
    __forceinline vec3_t& operator+=(const vec3_t& v) {
        x += v.x;
        y += v.y;
        z += v.z;
        return *this;
    }
    __forceinline vec3_t& operator-=(const vec3_t& v) {
        x -= v.x;
        y -= v.y;
        z -= v.z;
        return *this;
    }
    __forceinline vec3_t& operator*=(const vec3_t& v) {
        x *= v.x;
        y *= v.y;
        z *= v.z;
        return *this;
    }
    __forceinline vec3_t& operator/=(const vec3_t& v) {
        x /= v.x;
        y /= v.y;
        z /= v.z;
        return *this;
    }

    // arithmetic operators w/ float
    __forceinline vec3_t operator+(float f) const {
        return{
            x + f,
            y + f,
            z + f
        };
    }
    __forceinline vec3_t operator-(float f) const {
        return{
            x - f,
            y - f,
            z - f
        };
    }
    __forceinline vec3_t operator*(float f) const {
        return{
            x * f,
            y * f,
            z * f
        };
    }
    __forceinline vec3_t operator/(float f) const {
        return{
            x / f,
            y / f,
            z / f
        };
    }

    // compound assignment operators w/ float
    __forceinline vec3_t& operator+=(float f) {
        x += f;
        y += f;
        z += f;
        return *this;
    }
    __forceinline vec3_t& operator-=(float f) {
        x -= f;
        y -= f;
        z -= f;
        return *this;
    }
    __forceinline vec3_t& operator*=(float f) {
        x *= f;
        y *= f;
        z *= f;
        return *this;
    }
    __forceinline vec3_t& operator/=(float f) {
        x /= f;
        y /= f;
        z /= f;
        return *this;
    }

    // methods
    __forceinline void clear() {
        x = y = z = 0.f;
    }

    // methods for vector
    __forceinline float length_sqr() const {
        return ((x * x) + (y * y) + (z * z));
    }

    __forceinline float length_2d_sqr() const {
        return ((x * x) + (y * y));
    }

    __forceinline float length() const {
        return math::sqrt(length_sqr());
    }

    __forceinline float length_2d() const {
        return math::sqrt(length_2d_sqr());
    }

    __forceinline float dot(const vec3_t& v) const {
        return (x * v.x + y * v.y + z * v.z);
    }

    __forceinline vec3_t cross(const vec3_t &v) const {
        return{
            (y * v.z) - (z * v.y),
            (z * v.x) - (x * v.z),
            (x * v.y) - (y * v.x)
        };
    }

    __forceinline float normalize() {
        float len = length();
        (*this) /= (length() + std::numeric_limits< float >::epsilon());
        return len;
    }

    __forceinline vec3_t normalized() const {
        auto vec = *this;
        vec.normalize();
        return vec;
    }
};

__forceinline vec3_t operator*(float f, const vec3_t& v) {
    return v * f;
}

class __declspec(align(16)) vec_aligned_t : public vec3_t
{
public:
    vec_aligned_t() { }

    vec_aligned_t(const vec3_t& vec) {
        x = vec.x;
        y = vec.y;
        z = vec.z;
    }

    float w;
};

class matrix3x4_t {
public:
    matrix3x4_t() { }

    matrix3x4_t(float m00, float m01, float m02, float m03, float m10, float m11, float m12, float m13, float m20, float m21, float m22, float m23) {
        m_flMatVal[0][0] = m00;
        m_flMatVal[0][1] = m01;
        m_flMatVal[0][2] = m02;
        m_flMatVal[0][3] = m03;
        m_flMatVal[1][0] = m10;
        m_flMatVal[1][1] = m11;
        m_flMatVal[1][2] = m12;
        m_flMatVal[1][3] = m13;
        m_flMatVal[2][0] = m20;
        m_flMatVal[2][1] = m21;
        m_flMatVal[2][2] = m22;
        m_flMatVal[2][3] = m23;
    }

    void Init(vec3_t x, vec3_t y, vec3_t z, vec3_t origin) {
        m_flMatVal[0][0] = x.x;
        m_flMatVal[0][1] = y.x;
        m_flMatVal[0][2] = z.x;
        m_flMatVal[0][3] = origin.x;
        m_flMatVal[1][0] = x.y;
        m_flMatVal[1][1] = y.y;
        m_flMatVal[1][2] = z.y;
        m_flMatVal[1][3] = origin.y;
        m_flMatVal[2][0] = x.z;
        m_flMatVal[2][1] = y.z;
        m_flMatVal[2][2] = z.z;
        m_flMatVal[2][3] = origin.z;
    }

    matrix3x4_t(vec3_t x, vec3_t y, vec3_t z, vec3_t origin)
    {
        m_flMatVal[0][0] = x.x;
        m_flMatVal[0][1] = y.x;
        m_flMatVal[0][2] = z.x;
        m_flMatVal[0][3] = origin.x;
        m_flMatVal[1][0] = x.y;
        m_flMatVal[1][1] = y.y;
        m_flMatVal[1][2] = z.y;
        m_flMatVal[1][3] = origin.y;
        m_flMatVal[2][0] = x.z;
        m_flMatVal[2][1] = y.z;
        m_flMatVal[2][2] = z.z;
        m_flMatVal[2][3] = origin.z;
    }

    void SetOrigin(const vec3_t& p) {
        m_flMatVal[0][3] = p.x;
        m_flMatVal[1][3] = p.y;
        m_flMatVal[2][3] = p.z;
    }

    float* operator[](int i) {
        return m_flMatVal[i];
    }
    const float* operator[](int i) const {
        return m_flMatVal[i];
    }

    float* Base() {
        return &m_flMatVal[0][0];
    }
    const float* Base() const {
        return &m_flMatVal[0][0];
    }

public:
    float m_flMatVal[3][4];
};

class __declspec(align(16)) matrix3x4a_t : public matrix3x4_t
{
public:
    matrix3x4a_t& operator=(const matrix3x4_t& src) {
        util::copy(Base(), src.Base(), sizeof(float) * 3 * 4);
        return *this;
    };
};

class viewmatrix_t
{
public:
    float* operator[](int i) {
        return m[i];
    }
    const float* operator[](int i) const {
        return m[i];
    }

    float* Base() {
        return &m[0][0];
    }
    const float* Base() const {
        return &m[0][0];
    }

    void transform(vec3_t& dest)
    {
        auto v = dest;

        float iw = 1.f / (v.x * _14 + v.y * _24 + v.z * _34 + _44);
        dest.x = (v.x * _11 + v.y * _21 + v.z * _31 + _41) * iw;
        dest.y = (v.x * _12 + v.y * _22 + v.z * _32 + _42) * iw;
        dest.z = (v.x * _13 + v.y * _23 + v.z * _33 + _43) * iw;
    }

    void transform(vec3_t& dest, const vec3_t& v)
    {
        float iw = 1.f / (v.x * _14 + v.y * _24 + v.z * _34 + _44);
        dest.x = (v.x * _11 + v.y * _21 + v.z * _31 + _41) * iw;
        dest.y = (v.x * _12 + v.y * _22 + v.z * _32 + _42) * iw;
        dest.z = (v.x * _13 + v.y * _23 + v.z * _33 + _43) * iw;
    }

    void vec_transform(const vec3_t &in, vec3_t &out) {
        out = {
            in.dot(vec3_t(_11, _12, _13)) + _41,
            in.dot(vec3_t(_21, _22, _23)) + _42,
            in.dot(vec3_t(_31, _32, _33)) + _43
        };
    }
public:
    union
    {
        struct // Direct definition
        {
            float _11, _12, _13, _14;
            float _21, _22, _23, _24;
            float _31, _32, _33, _34;
            float _41, _42, _43, _44;
        };

        float m[4][4];
    };
};