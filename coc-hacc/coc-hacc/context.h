#pragma once

class Context
{
private:
public:

    Context();
    ~Context();

    void init();

    Actor* get_local_actor();
    GameLevel * get_level();
    RenderDevice * get_render_device();

    ERelationType get_relation(InventoryOwner* a, InventoryOwner* b);

    bool is_culled(const vec3_t & p);
    bool world_to_screen(const vec3_t& p, vec2_t& out);

};

extern Context g_ctx;