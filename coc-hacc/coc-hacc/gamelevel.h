#pragma once

class GameLevel
{
private:
    char pad_0000[32]; //0x0000
    class Actor* m_cur_ent; //0x0020
    class Actor* m_cur_view_ent; //0x0024
    char pad_0028[20]; //0x0028
    class CameraManager* m_cameras; //0x003C

public:
    __forceinline auto& get_cur_ent()
    {
        return m_cur_ent;
    }

    __forceinline auto get_camera()
    {
        return m_cameras;
    }
};
