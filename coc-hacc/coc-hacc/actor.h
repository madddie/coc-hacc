#pragma once

// predecl
class ScriptGameObject;

class Actor
{
private:
    char pad_0000[8]; //0x0000
    CLASS_ID m_cls_id; //0x0008
    char pad_0010[8]; //0x0010
    vec3_t m_origin; //0x0018
    char pad_0024[14]; //0x0024
    uint16_t m_id; //0x0032
    char pad_0034[28]; //0x0034
    viewmatrix_t m_xform; //0x0050
    char pad_0090[16]; //0x0090
    class CCF_Skeleton* m_form; //0x00A0
    char pad_00A4[4]; //0x00A4
    class str_value* m_classname_long; //0x00A8
    class str_value* m_classname; //0x00AC
    class str_value* m_model; //0x00B0
    char pad_00B4[164]; //0x00B4
    class ScriptGameObject* m_script_game_obj; //0x0154
    char pad_0158[164]; //0x0158
    class ActorCondition* m_condition; //0x0200
    char pad_0204[224]; //0x0204
    class Inventory* m_inv; //0x02E4
    char pad_02E8[700]; //0x02E8
    float m_run_coef; //0x05A0
    char pad_05A4[84]; //0x05A4
    uint32_t m_r_hand; //0x05FC
    uint32_t m_l_finger1; //0x0600
    uint32_t m_r_finger2; //0x0604
    uint32_t m_head; //0x0608
    uint32_t m_eye_left; //0x060C
    uint32_t m_eye_right; //0x0610
    uint32_t m_l_clavicle; //0x0614
    uint32_t m_r_clavicle; //0x0618
    uint32_t m_spine2; //0x061C
    uint32_t m_spine1; //0x0620
    uint32_t m_spine; //0x0624
    uint32_t m_neck; //0x0628
    char pad_062C[844]; //0x062C
    class ActorMemory* m_memory; //0x0978
    //char pad_0000[24]; //0x0000
    //vec3_t m_origin; //0x0018
    //char pad_0024[14]; //0x0024
    //uint16_t m_id; //0x0032
    //char pad_0034[28]; //0x0034
    //viewmatrix_t m_xform; //0x0050
    //char pad_0090[16]; //0x0090
    //class CCF_Skeleton* m_form; //0x00A0
    //char pad_00A4[4]; //0x00A4
    //class str_value* m_classname_long; //0x00A8
    //class str_value* m_classname; //0x00AC
    //class str_value* m_model; //0x00B0
    //char pad_00B4[332]; //0x00B4
    //class ActorCondition* m_condition; //0x0200
    //char pad_0204[228]; //0x0204
    //char pad_02E8[788]; //0x02E8
    //uint32_t m_r_hand; //0x05FC
    //uint32_t m_l_finger1; //0x0600
    //uint32_t m_r_finger2; //0x0604
    //uint32_t m_head; //0x0608
    //uint32_t m_eye_left; //0x060C
    //uint32_t m_eye_right; //0x0610
    //uint32_t m_l_clavicle; //0x0614
    //uint32_t m_r_clavicle; //0x0618
    //uint32_t m_spine2; //0x061C
    //uint32_t m_spine1; //0x0620
    //uint32_t m_spine; //0x0624
    //uint32_t m_neck; //0x0628
    //char pad_062C[844]; //0x062C
    //class ActorMemory* m_memory; //0x0978

public:

    // lol i pasted this from ida pseudo ;3
    // return (*(InventoryOwner (**)(void))(*(void**)this + 0x88))();
    // nvm
    __forceinline auto get_inventory_owner()
    {
        auto fn = util::get_method<InventoryOwner*(__thiscall*)(void*)>(this, 34);

        return fn(this);
    }

    __forceinline auto get_actor()
    {
        auto fn = util::get_method<Actor*(__thiscall*)(void*)>(this, 26);

        return fn(this);
    }

    __forceinline auto get_script_object()
    {
        return _safe_ptr(m_script_game_obj) ? m_script_game_obj : nullptr;
    }

    __forceinline auto get_run_coef()
    {
        return m_run_coef;
    }

    __forceinline void set_run_coef(float coef)
    {
        m_run_coef = coef;
    }

    __forceinline auto& get_position()
    {
        return m_origin;
    }

    __forceinline ActorMemory* get_memory()
    {
        return _safe_ptr(m_memory) ? m_memory : nullptr;
    }

    __forceinline auto get_id()
    {
        return m_id;
    }

    __forceinline auto get_class_id_to_name()
    {
        std::string ret;

        ret = _class_id_to_str(m_cls_id);

        return ret;
    }

    auto get_name()
    {
        auto str = get_classname();
        
        if (_safe_ptr(get_inventory_owner()))
        {
            str = get_inventory_owner()->get_name();
        }
        
        return str;
    }

    __forceinline std::string get_classname()
    {
        return Address::safe(m_classname) ? std::string(m_classname->get().data()) : "";
    }

    __forceinline auto get_condition()
    {
        return _safe_ptr(m_condition) ? m_condition : nullptr;
    }

    __forceinline auto& get_xform()
    {
        return m_xform;
    }

    __forceinline auto& get_skeleton()
    {
        return m_form;
    }

    __forceinline auto is_alive()
    {
        return get_condition() ? get_condition()->is_alive() : false;
    }

    __forceinline auto get_health()
    {
        return get_condition() ? get_condition()->get_health() : 0.f;
    }

    __forceinline auto get_max_health()
    {
        return get_condition() ? get_condition()->get_max_health() : 0.f;
    }
};