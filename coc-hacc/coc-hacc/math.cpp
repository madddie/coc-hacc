#include "inc.h"

void math::normalize_angle(float &angle) {
    float rot;

    // bad number.
    if (!std::isfinite(angle)) {
        angle = 0.f;
        return;
    }

    // no need to normalize this angle.
    if (angle >= -180.f && angle <= 180.f)
        return;

    // get amount of rotations needed.
    rot = std::round(std::abs(angle / 360.f));

    // normalize.
    angle = (angle < 0.f) ? angle + (360.f * rot) : angle - (360.f * rot);
}

void math::vector_angles(const vec3_t& forward, ang_t& angles, vec3_t *up) {
    vec3_t  left;
    float   len, up_z, pitch, yaw, roll;

    // get 2d length.
    len = forward.length_2d();

    if (up && (len > 0.001f)) {
        pitch = rad_to_deg(std::atan2(-forward.z, len));
        yaw = rad_to_deg(std::atan2(forward.y, forward.x));

        // get left direction vector using cross product.
        left = (*up).cross(forward).normalized();

        // calculate up_z.
        up_z = (left.y * forward.x) - (left.x * forward.y);

        // calculate roll.
        roll = rad_to_deg(std::atan2(left.z, up_z));
    }
    else {
        if (len > 0.f) {
            // calculate pitch and yaw.
            pitch = rad_to_deg(std::atan2(-forward.z, len));
            yaw = rad_to_deg(std::atan2(forward.y, forward.x));
            roll = 0.f;
        }
        else {
            pitch = (forward.z > 0.f) ? -90.f : 90.f;
            yaw = 0.f;
            roll = 0.f;
        }
    }

    // set out angles.
    angles = {pitch, yaw, roll};
}

void math::matrix_from_angles(const vec3_t& origin, const ang_t& angles, matrix3x4_t& out) {
    vec3_t fwd, right, up;

    math::angle_vectors(angles, &fwd, &right, &up);

    out[0][0] = fwd[0];
    out[1][0] = fwd[1];
    out[2][0] = fwd[2];
    out[0][1] = right[0];
    out[1][1] = right[1];
    out[2][1] = right[2];
    out[0][2] = up[0];
    out[1][2] = up[1];
    out[2][2] = up[2];
    out[0][3] = origin.x;
    out[1][3] = origin.y;
    out[2][3] = origin.z;
}

void math::angle_vectors(const ang_t& angles, vec3_t* forward, vec3_t* right /*= nullptr*/, vec3_t* up /*= nullptr */) {
    float cp = std::cos(deg_to_rad(angles.x)), sp = std::sin(deg_to_rad(angles.x));
    float cy = std::cos(deg_to_rad(angles.y)), sy = std::sin(deg_to_rad(angles.y));
    float cr = std::cos(deg_to_rad(angles.z)), sr = std::sin(deg_to_rad(angles.z));

    forward->x = cp * cy;
    forward->y = cp * sy;
    forward->z = -sp;

    if (right != nullptr) {
        right->x = -1.f * sr * sp * cy + -1.f * cr * -sy;
        right->y = -1.f * sr * sp * sy + -1.f * cr * cy;
        right->z = -1.f * sr * cp;
    }

    if (up != nullptr) {
        up->x = cr * sp * cy + -sr * -sy;
        up->y = cr * sp * sy + -sr * cy;
        up->z = cr * cp;
    }
}

void math::vector_rotate(const vec3_t &in, const matrix3x4_t &matrix, vec3_t &out) {
    out = {
        in.dot(vec3_t(matrix[0][0], matrix[0][1], matrix[0][2])),
        in.dot(vec3_t(matrix[1][0], matrix[1][1], matrix[1][2])),
        in.dot(vec3_t(matrix[2][0], matrix[2][1], matrix[2][2]))
    };
}

void math::vector_irotate(const vec3_t &in, const matrix3x4_t &matrix, vec3_t &out) {
    out = {
        in.x * matrix[0][0] + in.y * matrix[1][0] + in.z * matrix[2][0],
        in.x * matrix[0][1] + in.y * matrix[1][1] + in.z * matrix[2][1],
        in.x * matrix[0][2] + in.y * matrix[1][2] + in.z * matrix[2][2]
    };
}

void math::vector_transform(const vec3_t &in, const matrix3x4_t &matrix, vec3_t &out) {
    out = {
        in.dot(vec3_t(matrix[0][0], matrix[0][1], matrix[0][2])) + matrix[0][3],
        in.dot(vec3_t(matrix[1][0], matrix[1][1], matrix[1][2])) + matrix[1][3],
        in.dot(vec3_t(matrix[2][0], matrix[2][1], matrix[2][2])) + matrix[2][3]
    };
}

void math::vector_itransform(const vec3_t &in, const matrix3x4_t &matrix, vec3_t &out) {
    vec3_t new_in;

    // calculate new in vector.
    new_in = {
        in.x - matrix[0][3],
        in.y - matrix[1][3],
        in.z - matrix[2][3]
    };

    // calculate out.
    out = {
        new_in.x * matrix[0][0] + new_in.y * matrix[1][0] + new_in.z * matrix[2][0],
        new_in.x * matrix[0][1] + new_in.y * matrix[1][1] + new_in.z * matrix[2][1],
        new_in.x * matrix[0][2] + new_in.y * matrix[1][2] + new_in.z * matrix[2][2]
    };
}

float math::get_fov(const ang_t &view_angles, const vec3_t &start, const vec3_t &end) {
    vec3_t dir, fw;

    // get direction and normalize.
    dir = (end - start).normalized();

    // get the forward direction vector of the view angles.
    angle_vectors(view_angles, &fw);

    // get the angle between the view angles forward direction vector and the target location.
    return max< float >(rad_to_deg(std::acos(fw.dot(dir))), 0.f);
}

float math::get_fov(const ang_t &view_angles, const ang_t &new_angles)
{
    auto make_vector = [](const ang_t& ang, vec3_t& vec)
    {
        float p, y, tmp;

        p = deg_to_rad(ang.x);
        y = deg_to_rad(ang.y);

        tmp = std::cos(p);

        vec.x = float(-tmp * -std::cos(y));
        vec.y = float(std::sin(y) * tmp);
        vec.z = float(-std::sin(p));
    };

    vec3_t ang, aim;

    make_vector(view_angles, aim);
    make_vector(new_angles, ang);


    return rad_to_deg(std::acos(aim.dot(ang) / (aim.dot(aim))));
}

int math::intersect_ray_with_box(const vec3_t &start, const vec3_t &end_delta, const vec3_t &mins, const vec3_t &maxs, float tolerance) {
    int     side{-1};
    float   d1, d2, f, t1{-1.f}, t2{1.f};
    bool    start_solid{true};

    for (int i{}; i < 6; ++i) {
        if (i >= 3) {
            d1 = start[i - 3] - maxs[i - 3];
            d2 = d1 + end_delta[i - 3];
        }
        else {
            d1 = -start[i] + mins[i];
            d2 = d1 - end_delta[i];
        }

        // if completely in front of face, no intersection.
        if (d1 > 0.f && d2 > 0.f)
            return -1;

        // completely inside, check next face.
        if (d1 <= 0.f && d2 <= 0.f)
            continue;

        if (d1 > 0.f)
            start_solid = false;

        // crosses face
        if (d1 > d2) {
            f = d1 - tolerance;
            if (f < 0.f)
                f = 0.f;

            // f = Math::min< float >( f, 0.f );

            f = f / (d1 - d2);
            if (f > t1) {
                t1 = f;
                side = i;
            }
        }
        else {
            // leave.
            f = (d1 + tolerance) / (d1 - d2);
            if (f < t2)
                t2 = f;
        }
    }

    // we intersected something, return side.
    if (start_solid || (t1 < t2 && t1 >= 0.f))
        return side;

    return -1;
}

int math::intersect_ray_with_OBB(const vec3_t &start, const vec3_t &end_delta, vec3_t mins, vec3_t maxs, float radius, const matrix3x4_t &matrix) {
    vec3_t new_start, new_end_delta;

    // capsule -> AABB.
    if (radius != -1.f) {
        mins -= vec3_t(radius, radius, radius);
        maxs += vec3_t(radius, radius, radius);
    }

    // transform and rotate.
    vector_itransform(start, matrix, new_start);
    vector_irotate(end_delta, matrix, new_end_delta); // vector_rotate( end_delta, matrix, delta );

    return intersect_ray_with_box(new_start, new_end_delta, mins, maxs);
}

bool math::intersect_ray_with_sphere(const vec3_t &start, const vec3_t &ray_delta, const vec3_t &sphere_center, float radius) {
    vec3_t  sphere_to_ray;
    float   a, b, c, discrim;

    sphere_to_ray = start - sphere_center;
    a = ray_delta.dot(ray_delta);

    // this would occur in the case of a zero-length ray.
    if (a == 0.f)
        return (sphere_to_ray.length_sqr() <= radius * radius);

    b = 2.f * sphere_to_ray.dot(ray_delta);
    c = sphere_to_ray.dot(sphere_to_ray) - radius * radius;
    discrim = b * b - 4.f * a * c;

    return (discrim < 0.f) ? false : true;
}

viewmatrix_t& math::matrix_mul(const viewmatrix_t& A, const viewmatrix_t& B)
{
    auto m = A;

    m[0][0] = A.m[0][0] * B.m[0][0] + A.m[1][0] * B.m[0][1] + A.m[2][0] * B.m[0][2] + A.m[3][0] * B.m[0][3];
    m[0][1] = A.m[0][1] * B.m[0][0] + A.m[1][1] * B.m[0][1] + A.m[2][1] * B.m[0][2] + A.m[3][1] * B.m[0][3];
    m[0][2] = A.m[0][2] * B.m[0][0] + A.m[1][2] * B.m[0][1] + A.m[2][2] * B.m[0][2] + A.m[3][2] * B.m[0][3];
    m[0][3] = A.m[0][3] * B.m[0][0] + A.m[1][3] * B.m[0][1] + A.m[2][3] * B.m[0][2] + A.m[3][3] * B.m[0][3];

    m[1][0] = A.m[0][0] * B.m[1][0] + A.m[1][0] * B.m[1][1] + A.m[2][0] * B.m[1][2] + A.m[3][0] * B.m[1][3];
    m[1][1] = A.m[0][1] * B.m[1][0] + A.m[1][1] * B.m[1][1] + A.m[2][1] * B.m[1][2] + A.m[3][1] * B.m[1][3];
    m[1][2] = A.m[0][2] * B.m[1][0] + A.m[1][2] * B.m[1][1] + A.m[2][2] * B.m[1][2] + A.m[3][2] * B.m[1][3];
    m[1][3] = A.m[0][3] * B.m[1][0] + A.m[1][3] * B.m[1][1] + A.m[2][3] * B.m[1][2] + A.m[3][3] * B.m[1][3];

    m[2][0] = A.m[0][0] * B.m[2][0] + A.m[1][0] * B.m[2][1] + A.m[2][0] * B.m[2][2] + A.m[3][0] * B.m[2][3];
    m[2][1] = A.m[0][1] * B.m[2][0] + A.m[1][1] * B.m[2][1] + A.m[2][1] * B.m[2][2] + A.m[3][1] * B.m[2][3];
    m[2][2] = A.m[0][2] * B.m[2][0] + A.m[1][2] * B.m[2][1] + A.m[2][2] * B.m[2][2] + A.m[3][2] * B.m[2][3];
    m[2][3] = A.m[0][3] * B.m[2][0] + A.m[1][3] * B.m[2][1] + A.m[2][3] * B.m[2][2] + A.m[3][3] * B.m[2][3];

    m[3][0] = A.m[0][0] * B.m[3][0] + A.m[1][0] * B.m[3][1] + A.m[2][0] * B.m[3][2] + A.m[3][0] * B.m[3][3];
    m[3][1] = A.m[0][1] * B.m[3][0] + A.m[1][1] * B.m[3][1] + A.m[2][1] * B.m[3][2] + A.m[3][1] * B.m[3][3];
    m[3][2] = A.m[0][2] * B.m[3][0] + A.m[1][2] * B.m[3][1] + A.m[2][2] * B.m[3][2] + A.m[3][2] * B.m[3][3];
    m[3][3] = A.m[0][3] * B.m[3][0] + A.m[1][3] * B.m[3][1] + A.m[2][3] * B.m[3][2] + A.m[3][3] * B.m[3][3];

    return m;
}