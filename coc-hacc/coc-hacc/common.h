#pragma once

using CLASS_ID = uint64_t;

constexpr CLASS_ID _make_class_id
(
    char a,
    char b,
    char c,
    char d,
    char e,
    char f,
    char g,
    char h
) 
{
    return CLASS_ID
    (
        (CLASS_ID(a) << CLASS_ID(56)) | (CLASS_ID(b) << CLASS_ID(48)) | (CLASS_ID(c) << CLASS_ID(40)) | \
        (CLASS_ID(d) << CLASS_ID(32)) | (CLASS_ID(e) << CLASS_ID(24)) | (CLASS_ID(f) << CLASS_ID(16)) | \
        (CLASS_ID(g) << CLASS_ID(8)) | (CLASS_ID(h))
    );
}

//constexpr CLASS_ID _make_class_id(const std::array<char, sizeof(CLASS_ID)>& str)
//{
//    return _make_class_id(str[0], str[1], str[2], str[3], str[4], str[5], str[6], str[7]);
//}

constexpr char _get_byte(size_t which, uint64_t u)
{
    return (u >> (8 * which)) & 0xff;

}
constexpr std::array<char, sizeof(CLASS_ID) + 1> _class_id_to_char_arr(CLASS_ID id)
{
    return std::array<char, sizeof(CLASS_ID) + 1>
    {
        _get_byte(7, id),       
        _get_byte(6, id),        
        _get_byte(5, id),       
        _get_byte(4, id),       
        _get_byte(3, id),       
        _get_byte(2, id),       
        _get_byte(1, id),       
        _get_byte(0, id),
        0,
    };
}

#define _class_id_to_str(id) _class_id_to_char_arr(id).data()
#define _make_class_id_from_str(str) _make_class_id(str[0], str[1], str[2], str[3], str[4], str[5], str[6], str[7])

namespace BoneNames
{
    static std::string head = STR("bip01_head");
    static std::string neck = STR("bip01_neck");
    static std::string spine = STR("bip01_spine");
    static std::string spine1 = STR("bip01_spine1");
    static std::string spine2 = STR("bip01_spine2");
    static std::string pelvis = STR("bip01_pelvis");

    namespace LEFT
    {
        static std::string thigh = STR("bip01_l_thigh");
        static std::string calf = STR("bip01_l_calf");
        static std::string foot = STR("bip01_l_foot");
        static std::string toe = STR("bip01_l_toe0");
        static std::string upperarm = STR("bip01_l_upperarm");
        static std::string forearm = STR("bip01_l_forearm");
        static std::string hand = STR("bip01_l_hand");
        static std::string finger = STR("bip01_l_finger0");
    };

    namespace RIGHT
    {
        static std::string thigh = STR("bip01_r_thigh");
        static std::string calf = STR("bip01_r_calf");
        static std::string foot = STR("bip01_r_foot");
        static std::string toe = STR("bip01_r_toe0");
        static std::string upperarm = STR("bip01_r_upperarm");
        static std::string forearm = STR("bip01_r_forearm");
        static std::string hand = STR("bip01_r_hand");
        static std::string finger = STR("bip01_r_finger0");
    };
}

enum ERelationType
{
    eRelationTypeFriend = uint32_t(0),
    eRelationTypeNeutral,
    eRelationTypeEnemy,
    eRelationTypeWorstEnemy,
    eRelationTypeLast,
    eRelationTypeDummy = uint32_t(-1),
};

class InventoryOwner
{
private:
    char pad[0x24];
    uint32_t m_money;

public:
    
    __forceinline auto get_name()
    {
        auto fn = util::get_method<const char*(__thiscall*)(void*)>(this, 31);

        return fn(this);
    }

    __forceinline auto get_money()
    {
        return m_money;
    }

    __forceinline void set_money(uint32_t m)
    {
        m_money = m;
    }
};

class str_value
{
private:
    uint32_t m_ref_count; //0x0000
    uint32_t m_length; //0x0004
    uint32_t m_crc32; //0x0008
    class str_value* m_next; //0x000C
    std::array<char, 128> m_txt;

public:
    __forceinline auto& get()
    {
        return m_txt;
    }
}; //Size: 0x0050

class Fbox
{
private:
    vec3_t m_min; //0x0000
    vec3_t m_max; //0x000C

public:
    __forceinline auto& get_mins()
    {
        return m_min;
    }

    __forceinline auto& get_maxs()
    {
        return m_max;
    }

    vec3_t get_point(int index)
    {
        vec3_t result{};

        switch (index)
        {
        case 0: result = vec3_t{m_min.x, m_min.y, m_min.z}; break;
        case 1: result = vec3_t{m_min.x, m_min.y, m_max.z}; break;
        case 2: result = vec3_t{m_max.x, m_min.y, m_max.z}; break;
        case 3: result = vec3_t{m_max.x, m_min.y, m_min.z}; break;
        case 4: result = vec3_t{m_min.x, m_max.y, m_min.z}; break;
        case 5: result = vec3_t{m_min.x, m_max.y, m_max.z}; break;
        case 6: result = vec3_t{m_max.x, m_max.y, m_max.z}; break;
        case 7: result = vec3_t{m_max.x, m_max.y, m_min.z}; break;
        default: 
            break;
        }

        return result;
    }

}; //Size: 0x0018

class Fsphere
{
private:
    vec3_t m_pos; //0x0000
    float m_radius; //0x000C

public:
    __forceinline auto& get_pos()
    {
        return m_pos;
    }

    __forceinline auto get_radius()
    {
        return m_radius;
    }

}; //Size: 0x0010

class CamEffectorInfo
{
public:
    union
    {
        struct 
        {
            vec3_t m_pos; //0x0000
            vec3_t m_dir; //0x000C
            vec3_t m_up; //0x0018
            vec3_t m_right; //0x0024
        };
        viewmatrix_t m_viewmatrix;
    };
    float m_fov; //0x0030
    float m_far; //0x0034
    float m_aspect; //0x0038
    uint8_t m_dont_apply; //0x003C
    uint8_t m_affected_on_hud; //0x003D
}; //Size: 0x003E

class CameraManager
{
public:
    char pad_0000[4]; //0x0000
    CamEffectorInfo m_cam_info; //0x0004
}; //Size: 0x0084

class CCF_Skeleton
{
private:
    char pad_0000[4]; //0x0000
    class GameObject* m_owner; //0x0004
    uint32_t m_queryid; //0x0008
    Fbox m_bbox; //0x000C
    Fsphere m_sphere; //0x0024
    char pad_0034[4]; //0x0034
    uint64_t m_vis_mask; //0x0038
    class N00001CE0* N00001CDD; //0x0040
    char pad_0044[392]; //0x0044

public:
    __forceinline auto& get_bbox()
    {
        return m_bbox;
    }

    __forceinline auto& get_sphere()
    {
        return m_sphere;
    }

}; //Size: 0x01CC


class VisualMemoryManager
{
private:
    char pad_0000[36]; //0x0000
    char pad_0024[20]; //0x0024
    float m_min_view_distance; //0x0038
    float m_max_view_distance; //0x003C
    float m_visibility_threshold; //0x0040
    float m_always_vis_distance; //0x0044
    float m_time_quant; //0x0048
    float m_decrease_value; //0x004C
    float m_velocity_factor; //0x0050
    float m_transparency_threshold; //0x0054
    float m_lum_factor; //0x0058
    uint32_t m_still_vis_time; //0x005C
    char pad_0060[40]; //0x0060
    uint32_t m_max_object_count; //0x0088
}; //Size: 0x0144

class ActorCondition
{
private:
    char pad_0000[4]; //0x0000
    float m_health; //0x0004
    float m_max_health; //0x0008

public:
    
    __forceinline auto get_health()
    {
        return m_health < 0.f ? 0.f : m_health;
    }
    __forceinline bool is_alive()
    {
        return get_health() > 0.f;
    }

    __forceinline auto get_max_health()
    {
        return m_max_health;
    }
}; //Size: 0x0084

class ActorMemory
{
private:
    char pad_0000[6]; //0x0000
    uint16_t m_count; //0x0006
    char pad_0008[20]; //0x0008
    class Actor** m_actors; //0x001C
    char pad_0020[76]; //0x0020
    class VisualMemoryManager* m_vis_mem_manager; //0x006C

public:

    __forceinline auto get_count()
    {
        return m_count;
    }

    __forceinline Actor* get_actor(size_t index)
    {
        if (!m_actors || !Address::safe(m_actors))
            return nullptr;

        if (!Address::safe(m_actors[index]))
            return nullptr;

        return m_actors[index];
    }
}; //Size: 0x0084
