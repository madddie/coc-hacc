#include "inc.h"

void DirectWrite::init()
{
    // create factory
    D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_data.m_factory);

    // create dwrite factory
    DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), (IUnknown**) &m_data.m_directwrite_factory);

    // get backbuffer
    g_render.get_data().m_swap_chain->GetBuffer(0, __uuidof(m_data.m_backbuffer), (void**) &m_data.m_backbuffer);

    // get backbuffer render target view
    g_render.get_data().m_device->CreateRenderTargetView(m_data.m_backbuffer, nullptr, &m_data.m_backbuffer_rtv);
    
    // get backbuffer surface
    g_render.get_data().m_swap_chain->GetBuffer(0, __uuidof(m_data.m_back_buffer_surface), (void**)&m_data.m_back_buffer_surface);

    // proper dpi support
    float dpix, dpiy;
    m_data.m_factory->GetDesktopDpi(&dpix, &dpiy);

    // create render target properties
    auto properties = D2D1::RenderTargetProperties
    (
        D2D1_RENDER_TARGET_TYPE_DEFAULT, 
        D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED), 
        dpix,
        dpiy
    );

    // i patched the game to use BGRA
    // sig: 6A 00 6A 00 FF 76 7C] + 1 (patched to 0x20)

    // check flags
    auto flags = g_render.get_data().m_device->GetCreationFlags();
    g_con.print("[#] flags have BGRA %s\n", (flags & D3D11_CREATE_DEVICE_BGRA_SUPPORT) ? "-yep" : "-nope");

    // set swapchainbuffer

    // Wraps up our DXGI surface in a D2D render target.
    m_data.m_factory->CreateDxgiSurfaceRenderTarget(m_data.m_back_buffer_surface, &properties, &m_data.m_render_target);
  
    // create brush
    m_data.m_render_target->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::LimeGreen), &m_data.m_brush);

    // create fonts
    add_font(STR("Tahoma"));
    add_font(STR("Small Fonts"));
    add_font(STR("Operator Mono"));
}

