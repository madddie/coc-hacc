#pragma once

enum align_t
{
    LEFT,
    RIGHT,
    CENTER
};

class DirectWrite
{
public: 
    struct directwrite_data_t
    {
        HWND m_wnd;

        IDWriteFactory* m_directwrite_factory{};
        IDXGISurface* m_back_buffer_surface;
        ID2D1PathGeometry* m_path_geometry{};
        ID2D1GeometrySink* m_geometry_sink{};
        ID2D1Factory* m_factory{};
        ID2D1HwndRenderTarget * m_hwnd_render_target{};
        ID2D1RenderTarget* m_render_target;
        ID2D1SolidColorBrush* m_brush;

        ID3D11Resource* m_backbuffer;
        ID3D11RenderTargetView* m_backbuffer_rtv;
    } m_data;

    using font_map_t = std::unordered_map<size_t, IDWriteTextFormat*>;
    using font_t = std::pair<font_map_t, std::string>;
    using fonts_t = std::vector<font_t>;

    fonts_t m_fonts;

protected:

public:

    void init(); 

    font_map_t& get_font_map(const std::string& name)
    {
        return std::find_if(m_fonts.begin(), m_fonts.end(), [name](const font_t& a)
        {
            return a.second == name;
        })->first;
    }
      
    auto get_font(const std::string& name, size_t size = 10)
    {
        return get_font_map(name).at(size);
    }

    void add_font(const std::string& name, DWRITE_FONT_WEIGHT fw = DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE fs = DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH fst = DWRITE_FONT_STRETCH_EXPANDED)
    {
        font_t font{};

        // sizes of the fonts we wanna make
        constexpr std::array<size_t, 16> sizes =
        {
            8,
            9,
            10,
            11,
            12,
            14,
            16,
            18,
            20,
            22,
            24,
            26,
            28,
            36,
            48,
            72
        };

        // save the name
        font.second = name;

        // create font of every common size
        for (auto size : sizes)
        {
            IDWriteTextFormat* fmt;

            // create format
            m_data.m_directwrite_factory->CreateTextFormat
            (
                util::multibyte_to_wide(name).data(),
                nullptr,
                fw,
                fs,
                fst,
                size,
                L"en-US",
                &fmt
            );

            font.first[size] = fmt;
        }

        m_fonts.push_back(font);
    }

    void text(int x, int y, align_t align, const XMVECTORF32& color, IDWriteTextFormat* font, const std::wstringstream& str)
    {
        text(x, y, align, color, font, str.str());
    }
    void text(int x, int y, align_t align, const XMVECTORF32& color, IDWriteTextFormat* font, const std::string& str)
    {
        text(x, y, align, color, font, util::multibyte_to_wide(str));
    }
    void text(int x, int y, align_t align, const XMVECTORF32& color, IDWriteTextFormat* font, const std::wstring& str)
    {
        DWRITE_TEXT_METRICS metrics;
        IDWriteTextLayout*  layout;

        m_data.m_directwrite_factory->CreateTextLayout(str.data(), str.size(), font, g_render.get_width(), g_render.get_height(), &layout);
        layout->GetMetrics(&metrics);

        float layout_width = metrics.widthIncludingTrailingWhitespace, mod_width = layout_width;

        switch (align)
        {
        case RIGHT:
            mod_width *= 2.f;
            break;
        case LEFT:
            mod_width = 0.f;
            break;
        default:
            break;
        }

        D2D1_RECT_F bounds
        {
            float(x - (mod_width * .5f)),
            float(y),
            float((x - (mod_width * .5f)) + g_render.get_width()),
            float(y + g_render.get_height())
        };

        auto col = D2D1::ColorF(color[0], color[1], color[2], color[3]);

        m_data.m_brush->SetColor(col);

        m_data.m_render_target->BeginDraw();
        m_data.m_render_target->DrawTextW(str.data(), str.length(), font, bounds, m_data.m_brush, D2D1_DRAW_TEXT_OPTIONS_ENABLE_COLOR_FONT);
        m_data.m_render_target->EndDraw();
    }

    void outlined_text(int x, int y, align_t align, const XMVECTORF32& color_text, const XMVECTORF32& color_outline, IDWriteTextFormat* font, const std::string& str)
    {
        // this is so cheap
        text(x - 1, y, align, color_outline, font, str);
        text(x - 1, y - 1, align, color_outline, font, str);
        text(x - 1, y + 1, align, color_outline, font, str);
        text(x + 1, y, align, color_outline, font, str);
        text(x + 1, y - 1, align, color_outline, font, str);
        text(x + 1, y + 1, align, color_outline, font, str);
        text(x, y + 1, align, color_outline, font, str);
        text(x, y - 1, align, color_outline, font, str);

        text(x, y, align, color_text, font, str);
    }
};

extern DirectWrite g_dwrite;